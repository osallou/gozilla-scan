# gozilla-scan

Component of gozilla

in charge of scanning every day package files to get their
md5, size, etc. metadata

also update in stats the used storage for subjects

Default is *@daily*, can be overriden by env var GOZ_SCAN_INTERVAL.
Cf github.com/robfig/cron/v3 for interval syntax.
