FROM golang:latest

ADD ./* /root/
WORKDIR /root/
RUN go build -ldflags "-X main.Version=`git rev-parse --short HEAD`"

FROM alpine:latest
WORKDIR /root/
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2
COPY --from=0 /root/gozilla-scan /usr/bin/
COPY --from=0 /root/gozilla.yml /root/
