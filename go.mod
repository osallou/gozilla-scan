module gitlab.inria.fr/osallou/gozilla-scan

go 1.13

require (
	github.com/armon/go-metrics v0.3.3 // indirect
	github.com/elastic/go-elasticsearch v0.0.0
	github.com/gophercloud/gophercloud v0.9.0 // indirect
	github.com/hashicorp/consul/api v1.4.0 // indirect
	github.com/hashicorp/go-hclog v0.12.1 // indirect
	github.com/hashicorp/go-immutable-radix v1.2.0 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/hashicorp/serf v0.9.0 // indirect
	github.com/klauspost/compress v1.10.3 // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mitchellh/mapstructure v1.2.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/robfig/cron/v3 v3.0.1
	github.com/rs/zerolog v1.18.0
	gitlab.inria.fr/osallou/gozilla-lib v0.0.0-20200330151045-8a9b6337af3c
	go.mongodb.org/mongo-driver v1.3.1
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59 // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	golang.org/x/sys v0.0.0-20200327173247-9dae0f8f5775 // indirect
)
