package main

import (
	"context"
	"os"
	"os/signal"
	"time"

	"github.com/robfig/cron/v3"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	elasticsearch "github.com/elastic/go-elasticsearch"
	mongo "go.mongodb.org/mongo-driver/mongo"
	mongoOptions "go.mongodb.org/mongo-driver/mongo/options"

	gozilla "gitlab.inria.fr/osallou/gozilla-lib"
)

// Version of server
var Version string

func main() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	zerolog.SetGlobalLevel(zerolog.InfoLevel)

	config := gozilla.LoadConfig()

	mongoClient, err := mongo.NewClient(mongoOptions.Client().ApplyURI(config.Mongo.URL))
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}
	ctx, cancelMongo := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancelMongo()

	err = mongoClient.Connect(ctx)
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}

	storage, storageErr := gozilla.NewStorageHandler(config)
	if storageErr != nil {
		log.Error().Err(storageErr).Msg("storage error")
		os.Exit(1)
	}

	esCfg := elasticsearch.Config{
		Addresses: config.Elastic,
	}
	es, err := elasticsearch.NewClient(esCfg)
	if err != nil {
		log.Error().Err(err).Msgf("Error creating the client: %s", err)
		os.Exit(1)
	}

	gozCtx := gozilla.GozContext{
		Mongo:   mongoClient,
		Config:  config,
		Storage: storage,
		Elastic: es,
	}

	consulErr := gozilla.ConsulDeclare(gozCtx, "gozilla-scan")
	if consulErr != nil {
		log.Error().Err(consulErr).Msgf("Failed to register: %s", consulErr.Error())
		os.Exit(1)
	}

	c := cron.New(cron.WithChain(
		cron.SkipIfStillRunning(nil),
	))

	interval := os.Getenv("GOZ_SCAN_INTERVAL")
	if interval == "" {
		interval = "@daily"
	}
	c.AddFunc(interval, func() {

		p := gozilla.PackageVersionUpdate{}
		pvus, pvuErr := p.List(gozCtx)
		log.Info().Int("pending", len(pvus)).Msg("new scan")
		if pvuErr != nil {
			log.Error().Err(pvuErr).Msg("could not fetch updates")
		}
		for _, pvu := range pvus {
			err := pvu.Scan(gozCtx)
			if err != nil {
				log.Error().Err(err).Msg("scan failure")
			}
		}
	})
	c.Start()
	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt, os.Kill)
	<-sig
	log.Warn().Msg("exiting")
}
